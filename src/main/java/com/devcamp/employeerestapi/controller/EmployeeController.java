package com.devcamp.employeerestapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.employeerestapi.models.Employee;

@RestController
public class EmployeeController {
    @GetMapping("/employees")
    public ArrayList<Employee> getAllEmployees(){
        ArrayList<Employee> employees = new ArrayList<>();

        Employee emp1 = new Employee(1, "John", "Doe", 5000);
        Employee emp2 = new Employee(2, "Mary", "Smith", 6000);
        Employee emp3 = new Employee(3, "Tom", "Hanks", 7000);
        
        System.out.println(emp1);
        System.out.println(emp2);
        System.out.println(emp3);
        
        employees.add(emp1);
        employees.add(emp2);
        employees.add(emp3);
        
        return employees;
    }
}
